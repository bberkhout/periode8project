---
title: "Expressie van glucocorticoid receptoren"
author: "Benjamin Berkhout"
date: "May 9, 2019"
output: pdf_document
---
##Introductie
Corticosteroiden zijn hormonen die in de cortex van de nieren worden gemaakt. Deze steroiden kunnen ook gebruikt worden bij de behandeling van chronische ontstekings ziektes, zoals astma.
Hierbij wordt de ontstekingsreactie geremd omdat de corticosteroiden binden aan glucocorticoid receptors. Dit complex bindt vervolgens aan de pro-inflammatoire genen en remt deze zo. 
Ook wordt de acetylatie van deze genen omgekeerd, en worden anti-inflammatoire genen juist geacetyleerd. 

##Interactie van geneesmiddel en productie receptoren  
Bij afwezigheid van het geneesmiddel is de hoeveelheid mRNAr en receptoren (R) in evenwicht, er gaat een hoeveelheid mRNAr af en komt er weer bij, dit zorgt ook weer voor een toename en een afname van de hoeveelheid receptoren.
Bij het toedienen van het geneesmiddel (D) wordt dit systeem aangepast, het geneesmiddel gaat samen met de recpetoren (DR(N)) wat resulteert in een remmer (IC50_Rm), deze remmer heeft effect op de hoeveelheid mRNAr die wordt geproduceerd. Deze afname geeft hierdoor ook een afname in de productie van de receptoren. Een vermindering van het aantal receptoren zorgt daarentegen ook weer voor een afname van DR(N), dit zorgt voor een lagere productie van IC50_Rm, doordat er nu een lagere hoeveelheid IC50_Rm aanwezig is gaat de productie van mRNAr weer omhoog en gaat het hele proces zo weer verder.
```{r}
knitr::include_graphics("corticoFlow.png")
```


##Model 

```{r}
library(deSolve)

#Parameters for the model
params <- c(Ks_rm = 2.90, IC50_rm = 26.2, Kon = 0.00329,
            Kt = 0.63, Kre = 0.57, Rf = 0.49, Kd_r = 0.0572,
            Kd_rm = 0.612, Ks_r = 3.22, D = (20000/374.471))

#Defining initial state
state = c(Rm0 = 4.74, R0 = 267,
          DR = 0, DR.N = 0)

#runtime
times <- seq(0, 48, by=1)

cortico.model <- function(t, y, params){
  Rm0 <- y[1]
  R0 <- y[2]
  DR <- y[3]
  DR.N <- y[4]
  with(as.list(c(params)),{
    dmRNAr <- Ks_rm * (1-(DR.N/(IC50_rm+DR.N)))-Kd_rm * Rm0
    dR <- (Ks_r * Rm0) + (Rf * Kre * DR.N) - (Kon * D * R0) - (Kd_r * R0)
    dDR <- Kon * D * R0 - Kt * DR
    dDR.N <- Kt * DR - Kre * DR.N
    return(list(c(dmRNAr, dR, dDR, dDR.N)))
  }
       )
}

model  <- ode(time = times, y = state, parms = params, func = cortico.model, method = "euler")

plot(model, ylab=c("fmol/g","fmol/g","fmol/g","fmol/g"), xlab="time in hours")
```

##Resultaten

a) In de grafiek van Rm0 is te zien hoe de productie van mRNAr enorm afneemt na het toedienen van het geneesmiddel, door de resulterende vermindering van het aantal receptoren komt er minder remmer waardoor de productie mRNAr weer langzaam toeneemt.

b) Na het toedienen van het geneesmiddel worden alle geproduceerde receptoren omgezet naar DR. Dit is te zien in de grafiek, de hoeveelhied R neemt af en blijft vervolgens laag.

c) Als het geneesmiddel is toegediend worden alle receptoren omgezet naar DR en DR(N), dit is te zien in de stijging in de grafieken. Na een bepaalde tijd ontstaat er een evenwicht in het aantal geproduceerde en geremde DR en DR(N).

De vrije receptor is de belangrijkste variabele in de formule. De hoeveelheid receptoren heeft direct invloed op de gemaakte DR complexen, zonder aanwezigheid van receptoren kan het geneesmiddel niet zijn werk doen.

